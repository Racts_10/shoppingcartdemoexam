/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sheridan;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rachit Vyas
 */
public class CartTest {
    
    public CartTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        ShoppingCartDemo.main(args);
        fail("The test case is a prototype.");
    }
    @Test
    public void GoodTest()
    {
        System.out.println("Testing Regular: ");
        String product = "Tshirt";
        ShoppingCartDemo instance = new ShoppingCartDemo();
        boolean expResult = true;
        boolean result = ShoppingCartDemo.validateProduct(product);
        assertEquals(expResult, result);
        fail("Error");
    }
    @Test
    public void BadTest()
    {
        System.out.println("Testing Regular: ");
        String product = "Fruits";
        ShoppingCartDemo instance = new ShoppingCartDemo();
        boolean expResult = true;
        assertEquals("Bad Test",expResult);
    }
}
